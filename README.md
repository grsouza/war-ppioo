# War PPIOO

## Como contribuir?

* Faça um clone do repositório: `git clone https://gitlab.com/grsouza/warppioo-ppioo.git`
* Crie um branch para você: `git checkout -b "seu_nome"` **(NUNCA TRABALHE DIRETAMENTO NO BRANCH MASTER)**
* Adicione funcionalidades/corrija bugs e faça seus commits
* Dê um push no seu branch para o remote: `git push origin "nome_do_seu_branch"`
* Crie um merge request no GitLab