package warppioo;

enum Continente {
    AFRICA("AFR", 6),
    AMERICA_DO_NORTE("ADN", 7),
    AMERICA_DO_SUL("ADS", 4),
    ASIA("ASI", 7),
    EUROPA("EUR", 5),
    OCEANIA("OCE", 4);

    private final String nome;
    private final int qtdeTerritorios;

    private Continente(String nome, int qtdeTerritorios) {
        this.nome = nome;
        this.qtdeTerritorios = qtdeTerritorios;
    }

    public String getNome() {
        return nome;
    }

    public int getQtdeTerritorios() {
        return qtdeTerritorios;
    }

    public Continente[] getVizinhos() {
        switch (this) {
            case AFRICA:
                return new Continente[]{Continente.AMERICA_DO_NORTE, Continente.EUROPA, Continente.ASIA, Continente.OCEANIA};
            case AMERICA_DO_NORTE:
                return new Continente[]{Continente.AMERICA_DO_SUL, Continente.EUROPA, Continente.ASIA, Continente.AFRICA};
            case AMERICA_DO_SUL:
                return new Continente[]{Continente.AMERICA_DO_NORTE, Continente.ASIA, Continente.OCEANIA};
            case ASIA:
                return new Continente[]{Continente.AFRICA, Continente.AMERICA_DO_NORTE, Continente.AMERICA_DO_SUL, Continente.EUROPA, Continente.OCEANIA};
            case EUROPA:
                return new Continente[]{Continente.AMERICA_DO_NORTE, Continente.ASIA, Continente.AFRICA};
            case OCEANIA:
                return new Continente[]{Continente.AMERICA_DO_SUL, Continente.ASIA, Continente.AFRICA};
            default:
                return null;
        }
    }
}
