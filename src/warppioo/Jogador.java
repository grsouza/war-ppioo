package warppioo;

import java.util.ArrayList;
import java.util.HashMap;

public class Jogador {

    private int id;
    private String nome;
    private ArrayList<Territorio> territorios;

    public Jogador(String nome, int id) {
        this.nome = nome;
        this.id = id;
        territorios = new ArrayList();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void addTerritorio(Territorio t) {
        territorios.add(t);
        t.setJogador(this);
        t.addExercitoAereo();
        t.addExercitoTerreste();
    }

    //Diferente do addTerritorio(Territorio t)
    public void addTerritorioDepoisCombate(Territorio t) {
        territorios.add(t);
        t.setJogador(this);
    }

    public int getId() {
        return id;
    }

    public int exercitosTerrestresDisponiveis() {
        return (int) (territorios.size() / 2);
    }

    public int exercitosAereoDisponiveis() {
        return (int) (territorios.size() / 3);
    }

    public ArrayList<Territorio> getTerritorios() {
        return territorios;
    }

    public void addExercitos(int territorioId, int tipo, int qtdeExercitos) {
        Territorio t = this.territorios.get(territorioId);
        if (tipo == 0) {
            t.addExercitosTerrestre(qtdeExercitos);
        } else if (tipo == 1) {
            t.addExercitosAereo(qtdeExercitos);
        }
    }

    public ArrayList<Territorio> getTerritoriosDePossiveisCombate() {
        ArrayList<Territorio> resultado = new ArrayList();

        for (int i = 0; i < territorios.size(); i++) {
            Territorio t = territorios.get(i);
            if (t.getCountExercitosTerrestres() > 1 && t.getCountExercitosAereo() >= 1) {
                resultado.add(t);
            }
        }

        return resultado;
    }

    public ArrayList<Territorio> getTerritoriosPossiveisRemanejar() {
        ArrayList<Territorio> resultado = new ArrayList();

        for (int i = 0; i < territorios.size(); i++) {
            Territorio t = territorios.get(i);
            if (t.getCountExercitosTerrestres() > 1 && t.getCountExercitosAereo() >= 1) {
                resultado.add(t);
            }
        }

        return resultado;
    }


    public void removeTerritorio(Territorio t) {
        territorios.remove(t);
    }

    /**
     * Verifica se o jogador venceu o jogo.
     * Itera sobre todos os territórios atuais do jogador verificando se o mesmo possui mais de 2 continentes
     * em sua totalidade.
     *
     * @return boolean
     */
    public boolean venci() {
        HashMap<Continente, ArrayList<Territorio>> territoriosPorContinente = new HashMap();

        for (Continente continente : Continente.values()) {
            territoriosPorContinente.put(continente, new ArrayList());
        }

        // Distribuir territorios em seus respectivos continentes.
        for (Territorio territorio : territorios) {
            ArrayList ts = territoriosPorContinente.get(territorio.getContinente());
            ts.add(territorio);
            territoriosPorContinente.put(territorio.getContinente(), ts);
        }

        int nContinentes = 0;

        for (HashMap.Entry<Continente, ArrayList<Territorio>> entry : territoriosPorContinente.entrySet())
            if (entry.getKey().getQtdeTerritorios() == entry.getValue().size()) nContinentes++;

        return nContinentes >= 2;
    }
}
