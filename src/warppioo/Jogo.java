package warppioo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Jogo {
    private final Territorio[][] tabuleiro;
    private Jogador jogador;
    private Jogador jogador1;
    private Jogador jogador2;

    public Jogo() {
        tabuleiro = gerarTabuleiro();
    }

    private Territorio[][] gerarTabuleiro() {

        // América do Norte
        Territorio alasca = new Territorio("Alasca", Continente.AMERICA_DO_NORTE);
        Territorio vancouver = new Territorio("Vancouver", Continente.AMERICA_DO_NORTE);
        Territorio groenlandia = new Territorio("Groenlândia", Continente.AMERICA_DO_NORTE);
        Territorio california = new Territorio("California", Continente.AMERICA_DO_NORTE);
        Territorio otawa = new Territorio("Otawa", Continente.AMERICA_DO_NORTE);
        Territorio mexico = new Territorio("México", Continente.AMERICA_DO_NORTE);
        Territorio novaYork = new Territorio("Nova York", Continente.AMERICA_DO_NORTE);

        // América do Sul
        Territorio chile = new Territorio("Chile", Continente.AMERICA_DO_SUL);
        Territorio colombia = new Territorio("Colômbia", Continente.AMERICA_DO_SUL);
        Territorio argentina = new Territorio("Argentina", Continente.AMERICA_DO_SUL);
        Territorio brasil = new Territorio("Brasil", Continente.AMERICA_DO_SUL);

        // Europa
        Territorio inglaterra = new Territorio("Inglaterra", Continente.EUROPA);
        Territorio italia = new Territorio("Itália", Continente.EUROPA);
        Territorio suecia = new Territorio("Suêcia", Continente.EUROPA);
        Territorio alemanha = new Territorio("Alemanha", Continente.EUROPA);
        Territorio moscou = new Territorio("Moscou", Continente.EUROPA);

        // África
        Territorio nigeria = new Territorio("Nigéria", Continente.AFRICA);
        Territorio egito = new Territorio("Egito", Continente.AFRICA);
        Territorio congo = new Territorio("Congo", Continente.AFRICA);
        Territorio sudao = new Territorio("Sudão", Continente.AFRICA);
        Territorio africaDoSul = new Territorio("África do Sul", Continente.AFRICA);
        Territorio madagascar = new Territorio("Madagascar", Continente.AFRICA);

        // Ásia
        Territorio vladivostok = new Territorio("Vladivostok", Continente.ASIA);
        Territorio omsk = new Territorio("Omsk", Continente.ASIA);
        Territorio siberia = new Territorio("Sibéria", Continente.ASIA);
        Territorio orienteMedio = new Territorio("Oriente Médio", Continente.ASIA);
        Territorio india = new Territorio("Índia", Continente.ASIA);
        Territorio china = new Territorio("China", Continente.ASIA);
        Territorio japao = new Territorio("Japão", Continente.ASIA);

        // Oceania
        Territorio sumatra = new Territorio("Sumatra", Continente.OCEANIA);
        Territorio borneu = new Territorio("Bornéu", Continente.OCEANIA);
        Territorio australia = new Territorio("Austrália", Continente.OCEANIA);
        Territorio novaGuine = new Territorio("Nova Guiné", Continente.OCEANIA);

        return new Territorio[][]{
                {alasca, vancouver, groenlandia, inglaterra, italia, suecia, null, vladivostok},
                {null, california, otawa, null, alemanha, moscou, omsk, siberia},
                {null, mexico, novaYork, nigeria, egito, orienteMedio, india, china},
                {chile, colombia, null, congo, sudao, sumatra, borneu, japao},
                {argentina, brasil, null, africaDoSul, madagascar, null, australia, novaGuine}
        };
    }

    /**
     * Função para definir os jogadores do jogo atual.
     *
     * @param jogador1
     * @param jogador2
     */
    public void definirJogadores(Jogador jogador1, Jogador jogador2) {
        this.jogador1 = jogador1;
        this.jogador2 = jogador2;
    }

    public Jogador getJogador1() {
        return jogador1;
    }

    public Jogador getJogador2() {
        return jogador2;
    }

    /**
     * Função que realiza a distribuição inicial de territorios entre os jogadores.
     */
    public void distribuirTerritorios() {
        int random;

        do {
            for (Territorio[] linha : tabuleiro) {
                for (Territorio territorio : linha) {
                    if (territorio != null) {

                        random = (int) (Math.random() * 2);
                        if (random == 0) {
                            if (jogador1.getTerritorios().size() < 17) jogador1.addTerritorio(territorio);
                            else if (jogador2.getTerritorios().size() < 16) jogador2.addTerritorio(territorio);
                        } else if (random == 1) {
                            if (jogador2.getTerritorios().size() < 16) jogador2.addTerritorio(territorio);
                            else if (jogador1.getTerritorios().size() < 17) jogador1.addTerritorio(territorio);
                        }

                    }
                }
            }
        } while (fimDeJogo());

    }

    /**
     * Pega os vizinhos do território, na vertical e horizontal, considerando como se o tabuleiro fosse um cilindo.
     *
     * @param territorio
     * @return ArrayList de territorios vizinhos.
     */
    private ArrayList<Territorio> getVizinhanca(Territorio territorio) {
        ArrayList<Territorio> vizinhanca = new ArrayList();

        int i = 0, j = 0;

        outerLoop:
        for (int i0 = 0; i0 < tabuleiro.length; i0++) {
            for (int j0 = 0; j0 < tabuleiro[i0].length; j0++) {
                if (tabuleiro[i0][j0] != null) {
                    if (tabuleiro[i0][j0].equals(territorio)) {
                        i = i0;
                        j = j0;
                        break outerLoop;
                    }
                }
            }
        }

        for (int k = 0; k < 4; k++) {
            int l = (int) ((i + ((k < 2) ? Math.pow(-1, k) : 0) + tabuleiro.length) % tabuleiro.length);
            int m = (int) ((j + ((k > 1) ? Math.pow(-1, k) : 0) + tabuleiro[0].length) % tabuleiro[0].length);

            if ((i + l) != 4)
                if (tabuleiro[l][m] != null)
                    vizinhanca.add(tabuleiro[l][m]);
        }
        return vizinhanca;
    }

    public ArrayList<Territorio> getTerritoriosPossiveisDeSeremAtacados(Territorio t, Jogador atacante) {
        ArrayList<Territorio> vizinhanca = getVizinhanca(t);

        ArrayList<Territorio> possiveis = new ArrayList<>();

        for (Territorio vizinho : vizinhanca) {
            if (!vizinho.perterceAoJogador(atacante)) {
                possiveis.add(vizinho);
            }
        }

        return possiveis;
    }

    public ArrayList<Territorio> getTerritoriosPossiveisDeSeremAtacadosAereo(Territorio t, Jogador atacante) {
        ArrayList<Territorio> possiveisCandidatos = getVizinhanca(t);

        ArrayList<Territorio> possiveis = new ArrayList<>();

        for (Territorio possivel : possiveisCandidatos) {
            if (!possivel.perterceAoJogador(atacante) && possivel.getCountExercitosTerrestres() >= 4 && possivel.getCountExercitosAereo() > 0) {
                possiveis.add(possivel);
            }
        }

        return possiveis;
    }

    public ArrayList<Territorio> getTerritoriosPossiveisDestinos(Territorio t, Jogador jogador) {
        ArrayList<Territorio> possiveisCandidatos = getVizinhanca(t);

        ArrayList<Territorio> possiveis = new ArrayList<>();

        for (Territorio possivel : possiveisCandidatos) {
            if (possivel.perterceAoJogador(jogador)) {
                possiveis.add(possivel);
            }
        }

        return possiveis;
    }

    public ArrayList<Territorio> getAjudantesCombateAereo(Jogador atacante, Territorio territorio) {
        Continente[] continentesVizinhos = territorio.getContinente().getVizinhos();

        ArrayList<Territorio> possiveisAjudantes = new ArrayList<>();

        for (Continente continente : continentesVizinhos) {
            possiveisAjudantes.addAll(todosTerritoriosDoContinente(continente));
        }

        ArrayList<Territorio> possiveis = new ArrayList<>();

        for (Territorio possivel : possiveisAjudantes) {
            if (possivel.perterceAoJogador(atacante) && possivel.getCountExercitosAereo() > 0)
                possiveis.add(possivel);
        }

        return possiveis;
    }

    private ArrayList<Territorio> todosTerritoriosDoContinente(Continente continente) {
        ArrayList<Territorio> territorios = new ArrayList<>();

        for (Territorio[] linha : tabuleiro) {
            for (Territorio territorio : linha) {
                if (territorio != null)
                    if (territorio.getContinente().equals(continente))
                        territorios.add(territorio);
            }
        }

        return territorios;
    }

    public void combater(Territorio atacante, Territorio defensor, int qtdeExercitosAtaque) {
        int qtdeExercitosDefesa = defensor.getCountExercitosTerrestres();

        ArrayList<Integer> dadosDeAtaque = new ArrayList();
        ArrayList<Integer> dadosDeDefesa = new ArrayList();

        for (int i = 0; i < qtdeExercitosAtaque; i++)
            dadosDeAtaque.add(atacante.getExercitoTerrestre().combater());

        if (defensor.getCountExercitosTerrestres() > 3)
            qtdeExercitosDefesa = 3;

        for (int i = 0; i < qtdeExercitosDefesa; i++)
            dadosDeDefesa.add(defensor.getExercitoTerrestre().combater());

        Collections.sort(dadosDeAtaque);
        Collections.sort(dadosDeDefesa);

        Collections.reverse(dadosDeAtaque);
        Collections.reverse(dadosDeDefesa);

        int nCombates = dadosDeAtaque.size() < dadosDeDefesa.size() ? dadosDeAtaque.size() : dadosDeDefesa.size();

        for (int i = 0; i < nCombates; i++) {
            if (dadosDeAtaque.get(i) <= dadosDeDefesa.get(i)) {
                atacante.removerExercitoTerrestre();
                qtdeExercitosAtaque--;

            } else {
                defensor.removerExercitoTerrestre();
                if (defensor.getCountExercitosTerrestres() == 0) {
                    System.out.println("Território Dominado pelo atacante.");
                    defensor.getJogador().removeTerritorio(defensor);
                    atacante.getJogador().addTerritorioDepoisCombate(defensor);
                    defensor.setJogador(atacante.getJogador());
                    atacante.removerMaisDeUmExercitoTerrestre(qtdeExercitosAtaque);
                    defensor.addExercitosTerrestre(qtdeExercitosAtaque);
                    break;

                }
            }
        }

    }

    public void combateAereo(Territorio atacante, Territorio defensor, int qtdeExercitosAereo) {
        int dadoDefesa;
        int dadoAtaque;
        int i;
        boolean terminarAtaque = false;

        while (defensor.getCountExercitosAereo() > 0 && qtdeExercitosAereo > 0 && !terminarAtaque) {
            dadoDefesa = defensor.getExercitoAereo().combater();
            if (dadoDefesa > 0)
                qtdeExercitosAereo += dadoDefesa;

            for (i = 0; i < qtdeExercitosAereo; i++) {
                dadoAtaque = defensor.getExercitoAereo().combater();
                if (dadoAtaque > 0) {
                    defensor.removerExercitoAereo();
                    defensor.removerMaisDeUmExercitoTerrestre(dadoAtaque);
                }
            }
            if (defensor.getCountExercitosTerrestres() == 1 || defensor.getCountExercitosAereo() == 0) {
                System.out.println("Não é possível continuar o ataque aéreo");
                terminarAtaque = true;
            }
        }
    }

    public void remanejarExercitos(int tipo, Territorio territorioInicial, Territorio destino, int qtde) {
        if (tipo == 0) {
            if (territorioInicial.getCountExercitosTerrestres() - 1 >= qtde) {
                territorioInicial.removerMaisDeUmExercitoTerrestre(qtde);
                destino.addExercitosTerrestre(qtde);
            } else {
                System.out.println("Quantidade Inválida");
            }
        } else if (tipo == 1) {
            if (territorioInicial.getCountExercitosAereo() >= qtde) {
                territorioInicial.removerMaisDeUmExercitoAereo(qtde);
                destino.addExercitosAereo(qtde);
            } else {
                System.out.println("Quantidade Inválida");
            }
        } else {
            System.out.println("Tipo não existente");
        }
    }

    public boolean fimDeJogo() {
        return jogador1.venci() || jogador2.venci();
    }

    public Territorio[][] getTabuleiro() {
        return tabuleiro;
    }
}
