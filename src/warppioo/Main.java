package warppioo;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static int opcao;

    private static Scanner s = new Scanner(System.in);
    private static Jogo jogo = new Jogo();

    public static void main(String[] args) {

        do {
            menuPrincipal();

            opcao = s.nextInt();

            switch (opcao) {
                case 1:
                    menuAdicionarJogadores();
                    jogo.distribuirTerritorios();
                    iniciarRodada();
                    break;
                default:
                    System.out.println("KSHFLKJHLFJS");
                    break;
            }

        } while ((opcao != 0) && !jogo.fimDeJogo());

        if (jogo.getJogador1().venci())
            System.out.println("Parabéns " + jogo.getJogador1().getNome() + ", você ganhou!!!");
        else if (jogo.getJogador2().venci())
            System.out.println("Parabéns " + jogo.getJogador2().getNome() + ", você ganhou!!!");

        System.out.println("FIM DE JOGO!!!");
    }

    private static void menuPrincipal() {
        System.out.println("---------- MENU ----------");
        System.out.println("1) Jogar");
        System.out.println("0) Sair");
    }

    private static void menuAdicionarJogadores() {
        System.out.println("---------- ADICIONAR JOGADORES ----------");
        System.out.println("Jogador Preto");
        System.out.print("Nome: ");
        s.nextLine();
        Jogador jogador1 = new Jogador(s.nextLine(), 1);

        System.out.println();
        System.out.println("Jogador Branco");
        System.out.print("Nome: ");
        Jogador jogador2 = new Jogador(s.nextLine(), 2);

        jogo.definirJogadores(jogador1, jogador2);
    }

    private static void iniciarRodada() {
        System.out.println("==== Fase de Preparação ====");
        distribuirExercitos(jogo.getJogador1());
        distribuirExercitos(jogo.getJogador2());
        while (!jogo.fimDeJogo()) {
            distribuirExercitos(jogo.getJogador1());
            menuCombate(jogo.getJogador1());
            distribuirExercitos(jogo.getJogador2());
            menuCombate(jogo.getJogador2());
        }
    }

    /**
     * Função para realizar a distribuição de exércitos do jogador.
     *
     * @param jogador
     */
    private static void distribuirExercitos(Jogador jogador) {
        int qtdeExercitosPretoTerrestre = jogador.exercitosTerrestresDisponiveis();
        int qtdeExercitosPretoAereo = jogador.exercitosAereoDisponiveis();

        do {
            printarTabuleiro(jogo.getTabuleiro());
            System.out.println();

            System.out.println("Territorios de " + jogador.getNome());

            printarTerritorios(jogador.getTerritorios());

            System.out.println();

            System.out.println("Exercitos terrestres disponíveis: " + qtdeExercitosPretoTerrestre);
            System.out.println("Exercitos aereos disponíveis: " + qtdeExercitosPretoAereo);

            System.out.println();
            System.out.println("Escolha o tipo do exército que irá ser posicionado");
            System.out.println("0) Terrestre");
            System.out.println("1) Aereo");
            System.out.print("Tipo de exercito: ");
            int tipo = s.nextInt();

            System.out.println();

            System.out.print("Em qual territorio? ");
            int territorioId = s.nextInt();

            System.out.println();

            System.out.print("Quantidade de exercitos: ");
            int qtdeExercitos = s.nextInt();

            switch (tipo) {
                case 0:
                    if (qtdeExercitosPretoTerrestre >= qtdeExercitos) {
                        qtdeExercitosPretoTerrestre = qtdeExercitosPretoTerrestre - qtdeExercitos;

                        jogador.addExercitos(territorioId, tipo, qtdeExercitos);
                    } else {
                        System.out.println("Quantidade inválida.");
                    }
                    break;
                case 1:
                    if (qtdeExercitosPretoAereo >= qtdeExercitos) {
                        qtdeExercitosPretoAereo = qtdeExercitosPretoAereo - qtdeExercitos;

                        jogador.addExercitos(territorioId, tipo, qtdeExercitos);
                    } else {
                        System.out.println("Quantidade inválida.");
                    }
                    break;
                default:
                    System.out.println("Tipo de exercito inválido.");
                    break;
            }

        } while ((qtdeExercitosPretoAereo + qtdeExercitosPretoTerrestre) > 0);
    }

    private static void menuCombate(Jogador jogador) {
        do {
            printarTabuleiro(jogo.getTabuleiro());
            System.out.println();
            System.out.println("0) Combate terrestre");
            System.out.println("1) Combate aereo");
            System.out.println("2) Remanejar exercitos");
            System.out.println("3) Passar a vez");

            opcao = s.nextInt();

            switch (opcao) {
                case 0:
                    menuCombateTerrestre(jogador);
                    break;
                case 1:
                    menuCombateAereo(jogador);
                    break;
                case 2:
                    menuRemanejarExercitos(jogador);
                    break;
                default:
                    break;
            }
        } while ((opcao != 3) || (opcao != 2));
    }

    private static void menuCombateTerrestre(Jogador jogador) {
        ArrayList<Territorio> possiveisAtacantes = jogador.getTerritoriosDePossiveisCombate();
        printarTerritorios(possiveisAtacantes);
        System.out.println();

        System.out.print("Qual território irá partir o ataque? ");
        int territorioId = s.nextInt();

        Territorio atacante = possiveisAtacantes.get(territorioId);

        ArrayList<Territorio> possiveis = jogo.getTerritoriosPossiveisDeSeremAtacados(atacante, jogador);
        printarTerritorios(possiveis);
        if (possiveis.size() == 0) {
            System.out.println("A partir do território selecionado não há possibilidades de ataque.");
            return;
        }

        System.out.print("Qual território será atacado? ");
        territorioId = s.nextInt();

        Territorio defensor = possiveis.get(territorioId);

        int qtdeExercitosDisponiveis = atacante.getCountExercitosTerrestres() - 1;
        System.out.println("Exercitos disponíveis para ataque no territorio " + atacante.getNome() + " : " + qtdeExercitosDisponiveis);

        System.out.println("Quantidade de exercitos que irão atacar: ");
        int qtdeExercitos = s.nextInt();

        if (qtdeExercitos <= qtdeExercitosDisponiveis && qtdeExercitos <= 3) {
            jogo.combater(atacante, defensor, qtdeExercitos);
        } else {
            System.out.println("Quantidade de exercitos inválida.");
        }

        jogo.fimDeJogo();
    }

    private static void menuCombateAereo(Jogador jogador) {
        int qtdeExercitoAereo = 0;
        boolean ataquePronto = false;
        int territorioId;

        ArrayList<Territorio> possiveisAtacantes = jogador.getTerritoriosDePossiveisCombate();
        printarTerritorios(possiveisAtacantes);
        System.out.println("");
        System.out.print("Qual território irá partir o ataque? ");
        territorioId = s.nextInt();

        Territorio atacante = possiveisAtacantes.get(territorioId);

        ArrayList<Territorio> possiveis = jogo.getTerritoriosPossiveisDeSeremAtacadosAereo(atacante, jogador);
        printarTerritorios(possiveis);

        System.out.print("Qual território será atacado? ");
        territorioId = s.nextInt();

        Territorio defensor = possiveis.get(territorioId);

        while (qtdeExercitoAereo <= 3 || ataquePronto) {
            System.out.print("Qual(is) territórios o ataque será composto? ");
            System.out.println("Selecione um território que contenha pelo menos um exército aéreo");
            territorioId = s.nextInt();
        }
        jogo.combateAereo(atacante, defensor, qtdeExercitoAereo);
    }

    private static void menuRemanejarExercitos(Jogador jogador) {
        int territorioId;
        ArrayList<Territorio> possiveisParaRemanejar = jogador.getTerritoriosPossiveisRemanejar();
        printarTerritorios(possiveisParaRemanejar);

        System.out.println("");

        System.out.println("De qual território será retirado os exércitos?");
        territorioId = s.nextInt();
        Territorio territorioInicial = possiveisParaRemanejar.get(territorioId);

        System.out.println("Escolhao tipo do exército que irá ser remanejado");
        System.out.println("0) Terrestre");
        System.out.println("1) Aereo");
        System.out.print("Tipo de exercito: ");
        int tipo = s.nextInt();
        if (tipo == 0 || tipo == 1) {
            System.out.println("");
            System.out.print("Quantidade de exercitos: ");
            int qtdeExercitos = s.nextInt();

            ArrayList<Territorio> possiveis = jogo.getTerritoriosPossiveisDestinos(territorioInicial, jogador);
            printarTerritorios(possiveis);

            System.out.println("Insira o ID do território de destino dos exércitos.");
            territorioId = s.nextInt();
            Territorio destino = possiveis.get(territorioId);
            jogo.remanejarExercitos(tipo, territorioInicial, destino, qtdeExercitos);
        } else {
            System.out.println("Tipo Inválido");
        }
    }

    /**
     * Printa os territórios no terminal, motrando a quantidade de exercitos terrestres e aéreos no território.
     *
     * @param territorios
     */
    private static void printarTerritorios(ArrayList<Territorio> territorios) {
        for (int i = 0; i < territorios.size(); i++) {
            Territorio t = territorios.get(i);
            System.out.format("%-20s ", (i + ") " + t.getNome()));
            System.out.println("| " + t.getCountExercitosTerrestres() + " terrestres e " + t.getCountExercitosAereo() + " aereos");
        }
    }

    /**
     * Printa o tabuleiro no terminal.
     */
    private static void printarTabuleiro(Territorio[][] tabuleiro) {
        for (Territorio[] linha : tabuleiro) {
            for (Territorio territorio : linha) {
                if (territorio == null) {
                    System.out.format("| %-43s ", "");
                } else {
                    System.out.format("| %-13s [%3s] [J = %d] [T = %d] [A = %d] ",
                            territorio.getNome(),
                            territorio.getContinente().getNome(),
                            territorio.getJogador().getId(),
                            territorio.getCountExercitosTerrestres(),
                            territorio.getCountExercitosAereo()
                    );
                }
            }
            System.out.println("|");
        }
    }

}
