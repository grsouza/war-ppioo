package warppioo;

import java.util.ArrayList;

public class Queue<T> {

    private ArrayList<T> data;

    public Queue() {
        data = new ArrayList<T>();
    }

    public void enqueue(T value) {
        data.add(value);
    }

    public T dequeue() {
        if (!isEmpty())
            return data.remove(0);
        return null;
    }

    public boolean isEmpty() {
        return data.size() == 0;
    }

}
