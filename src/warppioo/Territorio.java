package warppioo;

import warppioo.Exercitos.Aereo;
import warppioo.Exercitos.Terrestre;

public class Territorio {
    private final String nome;
    private String cor;
    private final Continente continente;
    private Jogador jogador;
    private int countExercitosAereo = 0;
    private int countExercitosTerrestres = 0;

    public boolean visitado = false;

    private Terrestre exercitoTerrestre;
    private Aereo exercitoAereo;

    public Territorio(String nome, Continente continente) {
        this.nome = nome;
        this.continente = continente;
        exercitoTerrestre = new Terrestre();
        exercitoAereo = new Aereo();
    }

    public String getNome() {
        return nome;
    }

    public Continente getContinente() {
        return continente;
    }

    public Jogador getJogador() {
        return jogador;
    }

    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
    }

    public Terrestre getExercitoTerrestre() {
        return exercitoTerrestre;
    }

    public Aereo getExercitoAereo() {
        return exercitoAereo;
    }

    public int getCountExercitosAereo() {
        return countExercitosAereo;
    }

    public int getCountExercitosTerrestres() {
        return countExercitosTerrestres;
    }

    public void addExercitoAereo() {
        countExercitosAereo++;
    }

    public void addExercitoTerreste() {
        countExercitosTerrestres++;
    }

    public void addExercitosAereo(int qtde) {
        countExercitosAereo = countExercitosAereo + qtde;
    }

    public void addExercitosTerrestre(int qtde) {
        countExercitosTerrestres = countExercitosTerrestres + qtde;
    }

    public boolean perterceAoJogador(Jogador jogador) {
        return jogador.equals(this.jogador);
    }

    public void removerExercitoTerrestre() {
        countExercitosTerrestres--;
    }

    public void removerMaisDeUmExercitoTerrestre(int qtde) {
        countExercitosTerrestres -= qtde;
    }

    public void removerExercitoAereo() {
        countExercitosAereo--;
    }

    public void removerMaisDeUmExercitoAereo(int qtde) {
        countExercitosAereo = countExercitosAereo - qtde;
    }

}
